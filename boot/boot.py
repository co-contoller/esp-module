import gc, esp, network, urequests, socket, ujson, time

LOG_TAG = '[BOOT]'


def log(message, tag=True, nl=True):
    prefix = LOG_TAG + ' ' if tag else ''
    suffix = '\n' if nl else ''
    print(prefix + message, end=suffix)


class BootConfig:
    SERVER_INFO_ENDPOINT = "/info"
    SERVER_FILE_ENDPOINT = "/file"
    CONN_RETRY_LIMIT = 5
    CONN_RETRY_INTERVAL = 2
    CONFIG_NETWORK = "network"
    CONFIG_UPDATE = "update"

    def __init__(self):
        self.config = None
        self.load()

    def load(self):
        try:
            with open('boot.json', 'r') as file:
                self.config = ujson.load(file)
        except Exception as ex:
            log('ERROR: Cannot load boot config.')

    def store(self, update=False):
        try:
            if not self.config:
                self.load()
            if self.config:
                with open('boot.json', 'w') as file:
                    self.config[self.CONFIG_UPDATE]["ready"] = update
                    file.write(ujson.dumps(self.config))
            else:
                log('ERROR: Cannot load init config file.')
        except Exception as ex:
            log('ERROR: Cannot store boot config.')

    def is_update_ready(self):
        return self.config[self.CONFIG_UPDATE]["ready"]

    def get_network_cfg(self):
        network_cfg = self.config[self.CONFIG_NETWORK]
        return network_cfg["ssid"], network_cfg["password"]

    def get_update_host_path(self):
        url = self.config[self.CONFIG_UPDATE]["url"]
        try:
            proto, dummy, host, path = url.split("/", 3)
        except ValueError:
            proto, dummy, host = url.split("/", 2)
            path = ""
        return host, path


class Boot:

    def __init__(self):
        esp.osdebug(None)
        gc.collect()
        self.config = BootConfig()
        self.connection = None
        print('\n%s boot init..' % LOG_TAG)

    def start_bootloader(self):
        try:
            self.setup_network()
            self.download_files()
            self.config.store(False)
        except ValueError as ex:
            log('ERROR: ' + str(ex))
        finally:
            log('boot finished!\n')

    def setup_network(self):
        self.connection = network.WLAN(network.STA_IF)
        self.connection.active(True)
        if not self.connection.isconnected():
            log('no internet connection!')
            ssid, password = self.config.get_network_cfg()
            self.connection.connect(ssid, password)
        log('connecting to "%s" ' % self.connection.config('essid'), nl=False)
        for i in range(BootConfig.CONN_RETRY_LIMIT):
            if self.connection.isconnected():
                log('CONNECTED!', tag=False)
                return
            log('.', tag=False, nl=False)
            time.sleep(BootConfig.CONN_RETRY_INTERVAL)
        log('FAIL!', tag=False)
        raise Exception('Network. Client connection timeout!')

    def download_files(self):
        host, path = self.config.get_update_host_path()
        update_files_list = self.fetch_update_info(host, path + BootConfig.SERVER_INFO_ENDPOINT)
        log('got files in update %s' % len(update_files_list))
        for file in update_files_list:
            self.fetch_file(host, path + BootConfig.SERVER_FILE_ENDPOINT, file)

    def fetch_update_info(self, host, path):
        log('downloading files list..')
        try:
            response = urequests.get('http://%s/%s' % (host, path))
            files_list = response.json()['files']
            response.close()
            return files_list
        except Exception as ex:
            raise Exception('Network. Cannot fetch update info!')

    def fetch_file(self, host, path, file):
        dst_dir = ''
        log(' -"%s" file..' % file, nl=False)
        try:
            with open(dst_dir + '/' + file, 'w') as handler:
                client = self.get_client_socket(host, path + '/' + file)
                raw_data = False
                while True:
                    data_line = client.readline()
                    if not data_line:
                        break
                    if not raw_data and data_line == b'\r\n':
                        raw_data = True
                    elif raw_data:
                        handler.write(data_line)
                client.close()
                log(' DONE!', tag=False)
        except ValueError as ex:
            log(' FAIL! ' + str(ex), tag=False)

    def get_client_socket(self, host, path):
        address = socket.getaddrinfo(host, 80)[0][-1]
        client = socket.socket()
        client.connect(address)
        client.send(bytes('GET /%s HTTP/1.0\r\nHost: %s\r\n\r\n' % (path, host), 'utf8'))
        return client


if __name__ == "__main__":
    boot = Boot()
    if boot.config.is_update_ready():
        boot.start_bootloader()
    else:
        log("no update.\n")
    gc.collect()
