import ujson
from native import Btn


class Config:
    CFG_FILE_NAME = 'config.json'
    TRIGGER = 'trigger'
    MUTE_TIME = 'alarm_mute_time'
    NOISE_TIME = 'noise_time'
    BREAK_TIME = 'break_time'

    ALARM_WARN = 'warn_alarm'
    ALARM_DANGER = 'danger_alarm'

    def __init__(self):
        self.config = {}
        self.alarm_warn_trigger = 0
        self.alarm_danger_trigger = 0
        # self.store_cfg()
        self.load_cfg()

    def load_cfg(self):
        try:
            with open(self.CFG_FILE_NAME, 'r') as file:
                self.config = ujson.load(file)
                self.alarm_warn_trigger = self.config[self.ALARM_WARN][self.TRIGGER]
                self.alarm_danger_trigger = self.config[self.ALARM_DANGER][self.TRIGGER]
        except Exception as ex:
            print('ERR: Cannot load config. ' + str(ex))
            self.store_cfg()

    def store_cfg(self):
        try:
            with open(self.CFG_FILE_NAME, 'w') as file:
                config = self.get_config_dict()
                file.write(ujson.dumps(config))
                self.config = config
        except Exception as ex:
            print('ERR: Cannot store config. ' + str(ex))

    def is_alarm_stored(self, value, name):
        return value == self.config[name][self.TRIGGER]

    def get_alarm(self, name):
        return self.config[name]

    def get_mute_time(self):
        return self.config[self.MUTE_TIME]

    def get_config_dict(self):
        if self.alarm_danger_trigger <= self.alarm_warn_trigger:
            self.alarm_danger_trigger = self.alarm_warn_trigger + 1
        return {
            self.ALARM_WARN: {
                self.TRIGGER: self.alarm_warn_trigger,
                self.NOISE_TIME: 1,
                self.BREAK_TIME: 5
            },
            self.ALARM_DANGER: {
                self.TRIGGER: self.alarm_danger_trigger,
                self.NOISE_TIME: 0,
                self.BREAK_TIME: 0
            },
            self.MUTE_TIME: 60 * 15
        }


class Module:

    def __init__(self, main, parent, child=None):
        self.u_title = '---- ==== ----'
        self.l_title = self.u_title
        self.child_modules = child
        self.parent = parent
        self.child_idx = 0
        self.child_view = False
        self.set_child(main, self.child_idx)

    def use_child(self, main, idx):
        if self.child_modules:
            self.child_idx = idx
            self.child_modules[self.child_idx].init_self(main)

    def init_self(self, main):
        main.api.lcd_cls()
        main.api.lcd_print(0x01, self.u_title)
        self.set_child(main, self.child_idx)
        self.init(main)

    def set_child(self, main, idx):
        if self.child_modules:
            self.child_idx = idx
            txt = ' ' if idx == 0 else '<'
            txt += self.child_modules[self.child_idx].l_title
            txt += ' ' if idx == (len(self.child_modules) - 1) else '>'
            main.api.lcd_print(0x40, txt)

    def update_child(self, main):
        if not self.child_view:
            self.update_self(main)
        else:
            if main.api.btn.is_just_pressed(Btn.ESC):
                self.child_view = False
                self.init_self(main)
            self.child_modules[self.child_idx].update_child(main)

    def update_self(self, main):
        if self.child_modules:
            if main.api.btn.is_just_pressed(Btn.OK):
                self.child_view = True
                self.use_child(main, self.child_idx)
            elif main.api.btn.is_just_pressed(Btn.UP):
                if self.child_idx < (len(self.child_modules) - 1):
                    self.set_child(main, self.child_idx + 1)
            elif main.api.btn.is_just_pressed(Btn.DOWN):
                if self.child_idx > 0:
                    self.set_child(main, self.child_idx - 1)
        if self.parent.child_view:
            self.update(main)

    def init(self, main):
        pass

    def update(self, main):
        pass
