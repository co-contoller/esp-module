import time, urequests
from native import Btn
from util import Module, Config


class Main:

    def __init__(self):
        self.fan_enable = False
        self.alarm_muted = False

    def init_self(self, main):
        main.api.lcd_cls()
        main.api.lcd_print_val(0, main.api.temp_value)
        main.api.lcd_print(0x04, 'oC')
        self.display_fan(main, self.fan_enable)
        self.display_muted(main)

    def update_child(self, main):

        if main.alarm.triggered and not main.alarm.is_muted() and main.api.btn.is_just_pressed(Btn.UP or Btn.DOWN):
            main.alarm.mute(main)

        if main.alarm.is_muted() != self.alarm_muted:
            self.alarm_muted = main.alarm.is_muted()
            self.display_muted(main)

        if main.api.btn.is_just_pressed(Btn.OK):
            main.set_stage(main.module_idx + 1)

        if main.api.btn.is_just_pressed(Btn.ESC):
            self.display_fan(main, not self.fan_enable)

    def update_temp(self, main):
        main.api.lcd_print_val(0, main.api.temp_value)
        if main.alarm.is_muted():
            main.api.lcd_print(0x4b, str(main.alarm.muted_time))
        self.update_network_status(main)

    def update_network_status(self, main):
        try:
            response = urequests.post('http://home.shut.pl/controller/update', json=main.get_controller_request())
            main.parse_controller_response(response.json())
            response.close()
        except Exception as ex:
            print('Network. Cannot update status! %s' % str(ex))

    def display_fan(self, main, enable):
        self.fan_enable = enable
        fan_icon = '*' if self.fan_enable else ' '
        main.api.lcd_print(0x0F, fan_icon)
        main.api.enable_fan(self.fan_enable)

    def display_muted(self, main):
        mute_icon = 'M' if self.alarm_muted else ' '
        main.api.lcd_print(0x4F, mute_icon)


class Menu(Module):

    def __init__(self, main):
        super().__init__(main, main, [AlarmWarnSetting(main, self), AlarmDangerSetting(main, self)])
        self.u_title = '---- Menu ----'

    def update_self(self, main):
        if main.api.btn.is_just_pressed(Btn.ESC):
            main.set_stage(main.module_idx - 1)
        elif main.api.btn.is_just_pressed(Btn.OK):
            self.child_view = True
            self.use_child(main, self.child_idx)
        elif main.api.btn.is_just_pressed(Btn.UP):
            if self.child_idx < (len(self.child_modules) - 1):
                self.set_child(main, self.child_idx + 1)
        elif main.api.btn.is_just_pressed(Btn.DOWN):
            if self.child_idx > 0:
                self.set_child(main, self.child_idx - 1)
        else:
            self.update(main)


class AlarmWarnSetting(Module):
    ALARM_LIMIT = 110

    def __init__(self, main, parent):
        super().__init__(main, parent)
        self.u_title = '-- Alarm oC --'
        self.l_title = '  Warn Alarm  '

    def init(self, main):
        self.display_temp(main)

    def update(self, main):
        if main.api.btn.is_just_pressed(Btn.UP) and main.cfg.alarm_warn_trigger < self.ALARM_LIMIT:
            main.cfg.alarm_warn_trigger += 1
            self.display_temp(main)
        if main.api.btn.is_just_pressed(Btn.DOWN) and main.cfg.alarm_warn_trigger > 0:
            main.cfg.alarm_warn_trigger -= 1
            self.display_temp(main)
        if main.api.btn.is_just_pressed(Btn.OK):
            main.cfg.store_cfg()
            self.display_temp(main)

    def display_temp(self, main):
        main.api.lcd_print(0x42, '%doC  ' % main.cfg.alarm_warn_trigger)
        status = ' ' * 5 if main.cfg.is_alarm_stored(main.cfg.alarm_warn_trigger, Config.ALARM_WARN) else '(+/-)'
        main.api.lcd_print(0x49, status)


class AlarmDangerSetting(Module):
    ALARM_LIMIT = 120

    def __init__(self, main, parent):
        super().__init__(main, parent)
        self.u_title = '-- Alarm oC --'
        self.l_title = ' Danger Alarm '

    def init(self, main):
        self.display_temp(main)

    def update(self, main):
        if main.api.btn.is_just_pressed(Btn.UP) and main.cfg.alarm_danger_trigger < self.ALARM_LIMIT:
            main.cfg.alarm_danger_trigger += 1
            self.display_temp(main)
        if main.api.btn.is_just_pressed(Btn.DOWN) and main.cfg.alarm_danger_trigger > 0:
            main.cfg.alarm_danger_trigger -= 1
            self.display_temp(main)
        if main.api.btn.is_just_pressed(Btn.OK):
            main.cfg.store_cfg()
            self.display_temp(main)

    def display_temp(self, main):
        main.api.lcd_print(0x42, '%doC  ' % main.cfg.alarm_danger_trigger)
        status = ' ' * 5 if main.cfg.is_alarm_stored(main.cfg.alarm_danger_trigger, Config.ALARM_DANGER) else '(+/-)'
        main.api.lcd_print(0x49, status)
