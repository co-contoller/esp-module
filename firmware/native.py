import time
from machine import Pin, SPI


class Command:
    NOP = 0
    BUZZER_SET_INTERVAL = 2
    TEMP_CLR_FLAG = 3
    LCD_CLS = 4
    LCD_PRINT = 5
    LCD_PRINT_VALUE = 6


class Hardware:

    def __init__(self):
        self.spi = SPI(1, baudrate=100000, polarity=0, phase=0)
        self.header_in = bytearray(4)
        self.header_out = bytearray(b'\xF7\x00\x00\x00')
        self.payload_in = bytearray(0)
        self.payload_out = bytearray(0)
        self.submit_try = 0
        self.submit_flag = False

    def setup_header(self, api, cmd):
        self.header_out[1] = cmd
        self.header_out[2] = api.status_register

    def setup_buffer(self, size, value=b'\x00'):
        self.payload_out = bytearray(size)
        self.payload_out[:] = value * len(self.payload_out)

    def send_buffer(self, api, cmd):
        self.setup_header(api, cmd)
        self.spi.write_readinto(self.header_out, self.header_in)
        if self.header_in[0] == 0x7F and self.header_in[1] == 0x8F:
            self.parser_header(api)
            if self.payload_out:
                self.payload_in = bytearray(len(self.payload_out))
                self.spi.write_readinto(self.payload_out, self.payload_in)
                self.parse_payload()
        else:
            self.submit_try += 1
            print(self.header_out)
            print(self.header_in)
            print('ERR: spi connection corrupted')
            if self.submit_try < 3:
                print('ERR: retrying!')
                self.send_buffer(api, cmd)
            else:
                print('ERR: packet lost!')
        self.submit_try = 0
        self.payload_out = None
        self.submit_flag = True

    def parser_header(self, api):
        api.btn.update(self.header_in[2] & 0x0f)
        api.temp_update_flag = self.header_in[2] & 0x80
        if self.header_in[3] < 0x80:
            api.temp_value = self.header_in[3]
        else:
            api.temp_value = 0

    def parse_payload(self):
        pass


class Btn:
    OK = 0x01
    DOWN = 0x02
    UP = 0x04
    ESC = 0x08


class Api:

    def __init__(self):
        self.hw = Hardware()
        self.btn = Button()
        self.temp_value = 0
        self.temp_update_flag = False
        self.status_register = 0

    def update(self):
        if not self.hw.submit_flag:
            self.nop()
        self.hw.submit_flag = False
        time.sleep_ms(20)

    def nop(self):
        self.hw.send_buffer(self, Command.NOP)

    def lcd_cls(self):
        self.hw.send_buffer(self, Command.LCD_CLS)

    def lcd_print(self, location, message):
        self.hw.setup_buffer(17)
        str_buffer = bytes(message, 'utf-8')
        str_len = len(str_buffer)
        if str_len > 16:
            str_len = 16
        self.hw.payload_out[1:1+str_len] = str_buffer
        self.hw.payload_out[0] = location
        self.hw.send_buffer(self, Command.LCD_PRINT)

    def lcd_print_val(self, location, value):
        self.hw.setup_buffer(2)
        self.hw.payload_out[0] = location
        self.hw.payload_out[1] = value
        self.hw.send_buffer(self, Command.LCD_PRINT_VALUE)

    def temp_clr_flag(self):
        self.hw.send_buffer(self, Command.TEMP_CLR_FLAG)

    def buzzer_set_interval(self, noise_time, break_time):
        self.hw.setup_buffer(2)
        self.hw.payload_out[0] = noise_time
        self.hw.payload_out[1] = break_time
        self.hw.send_buffer(self, Command.BUZZER_SET_INTERVAL)

    def enable_buzzer(self, enable):
        self.int_set_status(0, enable)

    def enable_fan(self, enable):
        self.int_set_status(1, enable)

    def is_fan_enable(self):
        return self.status_register & (1 << 1)

    def int_set_status(self, bit, enable):
        if enable:
            self.status_register |= (1 << bit)
        else:
            self.status_register &= ~(1 << bit)


class Button:

    def __init__(self):
        self.btn_status = 0
        self.btn_last_status = 0

    def update(self, status):
        self.btn_status = status
        self.btn_last_status &= status

    def is_pressed(self, btn):
        return self.btn_status & btn

    def is_just_pressed(self, btn):
        state = False
        if self.btn_status & btn and not self.btn_last_status & btn:
            state = True
            self.btn_last_status |= btn
        return state
