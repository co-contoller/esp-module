from native import Command, Btn, Api
from util import Config
import stage
from boot import BootConfig


class Alarm:

    def __init__(self):
        self.alarm_name = ''
        self.triggered = False
        self.muted_time = 0

    def update(self, main):
        # print(main.api.temp_value)
        alarm_threshold_name = ''
        if main.api.temp_value >= main.cfg.get_alarm(Config.ALARM_DANGER)[Config.TRIGGER]:
            alarm_threshold_name = Config.ALARM_DANGER
        elif main.api.temp_value >= main.cfg.get_alarm(Config.ALARM_WARN)[Config.TRIGGER]:
            alarm_threshold_name = Config.ALARM_WARN
        if not alarm_threshold_name:
            self.alarm_end(main)
        elif alarm_threshold_name != self.alarm_name:
            self.alarm_triggered(main, alarm_threshold_name)
        if self.is_muted():
            self.muted_time -= 1
        main.api.enable_buzzer(self.triggered and not self.is_muted())

    def alarm_triggered(self, main, alarm):
        self.alarm_name = alarm
        alarm_cfg = main.cfg.get_alarm(alarm)
        main.api.buzzer_set_interval(alarm_cfg[Config.NOISE_TIME], alarm_cfg[Config.BREAK_TIME])
        self.triggered = True

    def alarm_end(self, main):
        self.alarm_name = ''
        self.triggered = False

    def mute(self, main):
        if not self.is_muted():
            self.muted_time = main.cfg.get_mute_time()

    def un_mute(self):
        self.muted_time = 0

    def is_muted(self):
        return self.muted_time > 0


class Controller:

    def __init__(self):
        self.api = Api()
        self.cfg = Config()
        self.alarm = Alarm()
        self.module_idx = 0
        self.modules = [stage.Main(), stage.Menu(self)]
        self.set_stage(0)
        self.req_tasks = []

    def set_stage(self, stage):
        self.module_idx = stage
        self.modules[self.module_idx].init_self(self)

    def update(self):

        if self.api.temp_update_flag:
            self.api.temp_clr_flag()
            self.on_update_temp()

        self.modules[self.module_idx].update_child(self)
        self.api.update()

    def on_update_temp(self):
        if self.module_idx == 0:
            self.modules[0].update_temp(self)
        self.alarm.update(self)

    def parse_controller_response(self, json):
        self.req_tasks = json['tasks']

    def get_controller_request(self):
        status = {
            "temp": self.api.temp_value,
            "fan": self.api.is_fan_enable(),
            "alarm": {
                "triggered": self.alarm.triggered,
                "name": self.alarm.alarm_name,
                "muted": self.alarm.is_muted(),
                "mute_time": self.alarm.muted_time
            }
        }
        for task in self.req_tasks:
            if task == "CONFIG":
                status["config"] = self.cfg.config
            elif task == "MUTE":
                self.alarm.mute(self)
            elif task == "UNMUTE":
                self.alarm.un_mute()
            elif task == "FAN_ON":
                self.api.enable_fan(True)
            elif task == "FAN_OFF":
                self.api.enable_fan(False)
            elif task == "REBOOT":
                import machine
                machine.reset()
            elif task == "UPDATE":
                self.update_soft()
        return status

    def update_soft(self):
        boot_config = BootConfig()
        boot_config.store(True)
        import machine
        machine.reset()

    @staticmethod
    def start():
        while True:
            controller.update()
        pass


controller = Controller()
controller.start()
